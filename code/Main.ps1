#	This is the main script and it makes sure that
#	Every script is run, so you only need to make sure that
#	You have every script downloaded, and have them located in the $home directory
#	This needs to be run AFTER the two basesetup scripts
#   To Make sure everything works, run this script as administrator

#   This builds the AD-Structur and does every step up until
#   Importing the GPOs, and linking them to the correct OU

#Creating the OUs, calls the script 1_MovingToOUs.ps1
.\1_CreatingOUs.ps1

#Adding users, calls the script 2_CreateUsersCSV.ps1
.\2_AddingUsers.ps1

#Making groups, calls the script 3_MovingUsersToGroups.ps1
.\3_MakingGroups.ps1

#Importing the GPOs, which calls the scripts 4_MapGuidToGpoName.ps1 and 4_GPOStructure.ps1
.\4_GPOImport.ps1
