#   This script moves the computers in the domain
#   To the correct OU from the orgchart
#   Run on Domain Controller, DC1
#   After joining all the computers to the domain
#   And making the OU structure

#   Moving Computers and Server to OU
Get-ADComputer "MGR" | Move-ADObject -TargetPath "OU=Adm,OU=Klienter,DC=onoff, DC=co"
Get-ADComputer "CL1" | Move-ADObject -TargetPath "OU=IT,OU=Klienter,DC=onoff, DC=co"
Get-ADComputer "SRV1" | Move-ADObject -TargetPath "OU=Servere,DC=onoff, DC=co"
