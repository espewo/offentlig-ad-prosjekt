# This script adds all users to their correct group
# Needs to be run logged in as Domain Administrator
# The script assumes that there are users in the OUs, and pre-existing groups
# If not, no user will be added to any group.
# The script needs to be run in regular powershell and not powershell core
# It will not work otherwise

$Users = $Null

$OUPath = @('OU=Markedsføring, OU=Brukere, DC=onoff, DC=co',
            'OU=AdmØko,OU=Brukere,DC=onoff,DC=co',
            'OU=Nettverk,OU=IT,OU=Brukere,DC=onoff,DC=co',
            'OU=Applikasjon,OU=IT,OU=Brukere,DC=onoff,DC=co',
            'OU=Brukerstøtte,OU=IT,OU=Brukere,DC=onoff,DC=co',
            'OU=IT,OU=Brukere,DC=onoff,DC=co'
            )

$group = @('g_Markedsføring', 'g_AdmØko'
           'g_Nettverk', 'g_Applikasjon'
           'g_Brukerstøtte', 'g_IT'
            )

#Importing Active Directory module.
Import-Module ActiveDirectory

function addUsers{
    param(
        $usersAdd,$addToGroup
    )

    foreach($user in $usersAdd)
    {
        Add-ADgroupMember -Identity $addToGroup -Members "$($user.DistinguishedName)"`
            -ErrorAction SilentlyContinue
    }

}

foreach($i in 0..$(($group.Length)-1))
{
    $Users = Get-ADUser -SearchBase "$($OUPath[$i])" -Filter *
    addUsers -usersAdd $Users -addToGroup "$($group[$i])"
}
