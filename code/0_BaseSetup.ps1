#   Installing chocolatey by Erik Hjelmset erikhje@ansatt.ntnu.no

#   This script needs to be run as Administrator
#   This script installs chocolatey
#   And uses chocolatey to install powershell-core, sysinternals and 7zip

#   The script also downloads the GPOs in a zip file from git repo.
#   And at last the script downloads all the scripts being used to setup the structure

#Installing chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
$TempFile = "tmp$(Get-Random).ps1"
(New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1') > $TempFile
& .\$TempFile

#Installing powershell-core
choco install -y powershell-core

#Installing sysinternals
choco install -y sysinternals

#Installing 7zip
choco install -y 7zip

exit