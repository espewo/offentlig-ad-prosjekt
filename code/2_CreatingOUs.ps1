#	This script creates the Organizational Units
#	Following the Organizational chart
#	For the company ONOFF.co
#	The script also moves every domain host to their correct OU

#User OUs
New-ADOrganizationalUnit 'Brukere' -Description 'Contains OUs and Users'
New-ADOrganizationalUnit 'Markedsføring' -Description 'Marketing team'`
	-Path 'OU=Brukere ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'AdmØko' -Description 'Administrasjon og Økonomi'`
	-Path 'OU=Brukere ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'IT' -Description 'Inndeling av IT Teams'`
	-Path 'OU=Brukere ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'Nettverk' -Description 'Administrere nettverk'`
	-Path 'OU=IT ,OU=Brukere ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'Applikasjon' -Description 'Drifter applikasjoner og servere'`
	-Path 'OU=IT ,OU=Brukere ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'Brukerstøtte' -Description 'Brukerstøtte Team'`
	-Path 'OU=IT ,OU=Brukere ,DC=onoff, DC=co'

#Computer OUs
New-ADOrganizationalUnit 'Klienter' -Description 'Contains OUs and users computers'
New-ADOrganizationalUnit 'Markedsføring' -Description 'Marketing team laptops'`
	-Path 'OU=Klienter ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'Adm' -Description 'Adm laptops and computers'`
	-Path 'OU=Klienter ,DC=onoff, DC=co'
New-ADOrganizationalUnit 'IT' -Description 'IT laptops and computers'`
	-Path 'OU=Klienter ,DC=onoff, DC=co'
#Server
New-ADOrganizationalUnit 'Servere' -Description 'Contains OUs and servers'

#Moving to correct OU
.\1_MovingToOUs.ps1
