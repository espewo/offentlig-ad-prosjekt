# This script imports all the GPOs in the GPOs folder to active directory
# Credit to the developer at microsoft for creating the script as part of their
# Baseline security GPO import
# We use these scripts to import our own GPOs that are based on the baseline security GPOs
# From Microsoft

#Extracting zip file content using 7zip
7z x .\GPOs.zip

#Moves to the correct folder for the script to work
Move-Item .\GPOs\ ..

$GpoMap = .\4_MapGuidToGpoName.ps1 ..\GPOs

Write-Output "Importing the following GPOs:" -ForegroundColor Cyan
$GpoMap.Keys | ForEach-Object { Write-Output $_ -ForegroundColor Cyan }

$rootDir = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Path)
$parentDir = [System.IO.Path]::GetDirectoryName($rootDir)
$gpoDir = [System.IO.Path]::Combine($parentDir, "GPOs")

$GpoMap.Keys | ForEach-Object {
    $key = $_
    $guid = $GpoMap[$key]
    Write-Output ($guid + ": " + $key) -ForegroundColor Cyan
    Import-GPO -BackupId $guid -Path $gpoDir -TargetName "$key" -CreateIfNeeded
}
#Implementing GPOs to AD-Structure
Write-Output "Setting Up GPO Structure"
.\4_GPOStructure.ps1
