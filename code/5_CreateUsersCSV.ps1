# This script is based on Eirik Hjelmås CreateUsersCSV.ps1 with some changes so it
# fits our company and AD structure.

# Each time the script is run, it will create a new random combination
# of firstname (which is also the username), lastname and department
# New unique random passwords are generated for every user

# Test so we don't overwrite a file by accident
#
if ((Get-ChildItem -ErrorAction SilentlyContinue onoffusers.csv).Exists)
  {"You alread have the file onoffusers.csv!"; return;}
if ($PSVersionTable.PSVersion.Major -eq 5)
  {Write-Output "This script cannot be executed in Windows PowerShell, please use PowerShell core"; return;}

# 100 unique firstnames without norwegian characters ('øæå')
#
$FirstName = @("Nora","Emma","Ella","Maja","Olivia","Emilie","Sofie","Leah",
               "Sofia","Ingrid","Frida","Sara","Tiril","Selma","Ada","Hedda",
               "Amalie","Anna","Alma","Eva","Mia","Thea","Live","Ida","Astrid",
               "Ellinor","Vilde","Linnea","Iben","Aurora","Mathilde","Jenny",
               "Tuva","Julie","Oda","Sigrid","Amanda","Lilly","Hedvig",
               "Victoria","Amelia","Josefine","Agnes","Solveig","Saga","Marie",
               "Eline","Oline","Maria","Hege","Jakob","Emil","Noah","Oliver",
               "Filip","William","Lucas","Liam","Henrik","Oskar","Aksel",
               "Theodor","Elias","Kasper","Magnus","Johannes","Isak","Mathias",
               "Tobias","Olav","Sander","Haakon","Jonas","Ludvig","Benjamin",
               "Matheo","Alfred","Alexander","Victor","Markus","Theo",
               "Mohammad","Herman","Adam","Ulrik","Iver","Sebastian","Johan",
               "Odin","Leon","Nikolai","Even","Leo","Kristian","Mikkel",
               "Gustav","Felix","Sverre","Adrian","Lars"
              )

# 100 unique lastnames
#
$LastName = @("Hansen","Johansen","Olsen","Larsen","Andersen","Pedersen",
              "Nilsen","Kristiansen","Jensen","Karlsen","Johnsen","Pettersen",
              "Eriksen","Berg","Haugen","Hagen","Johannessen","Andreassen",
              "Jacobsen","Dahl","Jørgensen","Henriksen","Lund","Halvorsen",
              "Sørensen","Jakobsen","Moen","Gundersen","Iversen","Strand",
              "Solberg","Svendsen","Eide","Knutsen","Martinsen","Paulsen",
              "Bakken","Kristoffersen","Mathisen","Lie","Amundsen","Nguyen",
              "Rasmussen","Ali","Lunde","Solheim","Berge","Moe","Nygård",
              "Bakke","Kristensen","Fredriksen","Holm","Lien","Hauge",
              "Christensen","Andresen","Nielsen","Knudsen","Evensen","Sæther",
              "Aas","Myhre","Hanssen","Ahmed","Haugland","Thomassen",
              "Sivertsen","Simonsen","Danielsen","Berntsen","Sandvik",
              "Rønning","Arnesen","Antonsen","Næss","Vik","Haug","Ellingsen",
              "Thorsen","Edvardsen","Birkeland","Isaksen","Gulbrandsen","Ruud",
              "Aasen","Strøm","Myklebust","Tangen","Ødegård","Eliassen",
              "Helland","Bøe","Jenssen","Aune","Mikkelsen","Tveit","Brekke",
              "Abrahamsen","Madsen"
             )

# 4 in Markedsføring, 4 in AdmØko, 10 in Nettverk, 10 in Applikasjon og 12 in Brukerstøtte
# 3 teamledere for hvert team i IT
$OrgUnits = @("ou=Markedsføring,ou=Brukere","ou=Markedsføring,ou=Brukere",
            "ou=Markedsføring,ou=Brukere","ou=Markedsføring,ou=Brukere",
            "ou=AdmØko,ou=Brukere","ou=AdmØko,ou=Brukere",
            "ou=AdmØko,ou=Brukere","ou=AdmØko,ou=Brukere",
            "ou=IT,ou=Brukere","ou=IT,ou=Brukere","ou=IT,ou=Brukere",
            "ou=Nettverk,ou=IT,ou=Brukere","ou=Nettverk,ou=IT,ou=Brukere",
            "ou=Nettverk,ou=IT,ou=Brukere","ou=Nettverk,ou=IT,ou=Brukere",
            "ou=Nettverk,ou=IT,ou=Brukere","ou=Nettverk,ou=IT,ou=Brukere",
            "ou=Nettverk,ou=IT,ou=Brukere","ou=Nettverk,ou=IT,ou=Brukere",
            "ou=Nettverk,ou=IT,ou=Brukere",
            "ou=Applikasjon,ou=IT,ou=Brukere","ou=Applikasjon,ou=IT,ou=Brukere",
            "ou=Applikasjon,ou=IT,ou=Brukere","ou=Applikasjon,ou=IT,ou=Brukere",
            "ou=Applikasjon,ou=IT,ou=Brukere","ou=Applikasjon,ou=IT,ou=Brukere",
            "ou=Applikasjon,ou=IT,ou=Brukere","ou=Applikasjon,ou=IT,ou=Brukere",
            "ou=Applikasjon,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere","ou=Brukerstøtte,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere","ou=Brukerstøtte,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere","ou=Brukerstøtte,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere","ou=Brukerstøtte,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere","ou=Brukerstøtte,ou=IT,ou=Brukere",
            "ou=Brukerstøtte,ou=IT,ou=Brukere"
             )


# Three shuffled indices to randomly mix firstname, lastname, and department
#
$fnidx = 0..39 | Get-Random -Shuffle
$lnidx = 0..39 | Get-Random -Shuffle
$ouidx = 0..39 | Get-Random -Shuffle

Write-Output "UserName;GivenName;SurName;UserPrincipalName;DisplayName;Password;Department;Path" > onoffusers.csv

foreach ($i in 0..39) {
  $UserName          = $FirstName[$fnidx[$i]].ToLower()
  $GivenName         = $FirstName[$fnidx[$i]]
  $SurName           = $LastName[$lnidx[$i]]
  $UserPrincipalName = $UserName + '@' + 'onoff.co'
  $DisplayName       = $GivenName + ' ' + $SurName
  $Password          = -join ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!"#$%&()*+,-./:<=>?@[\]_{|}'.ToCharArray() | Get-Random -Count 16) + '1.aA'
  $Department        = ($OrgUnits[$ouidx[$i]] -split '[=,]')[1]
  $Path              = $OrgUnits[$ouidx[$i]] + ',' + "DC=onoff, DC=co"
  Write-Output "$UserName;$GivenName;$SurName;$UserPrincipalName;$DisplayName;$Password;$Department;$Path" >> onoffusers.csv
}
