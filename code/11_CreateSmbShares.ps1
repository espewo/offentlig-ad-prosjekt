# This script makes Smb-Shares for each domain group that we made with
# the MakingGroups script.

# Making the directories for the fileshares.
New-Item -Path "C:\" -Name "fileshares" -ItemType Directory
Set-Location -Path C:\fileshares
"Markedsføring", "AdmØko", "IT", "Nettverk", "Applikasjon", "Brukerstøtte" | ForEach-Object{New-Item -Name "$_" -ItemType Directory}

# Creating fileshares for each group
$Fileshare = @{
    Name = 'Markedsføring'
    Path = 'C:\fileshares\Markedsføring'
    ChangeAccess = 'dl_Markedsføring'
    FullAccess = 'Adminstrators'
    Description = 'Marketing file share'
}
New-SmbShare @Fileshare

$Fileshare = @{
    Name = 'AdmØko'
    Path = 'C:\fileshares\AdmØko'
    ChangeAccess = 'dl_AdmØko'
    FullAccess = 'Administrators'
    Description = 'Administration file share'
}
New-SmbShare @Fileshare

$Fileshare = @{
    Name = 'IT'
    Path = 'C:\fileshares\IT'
    ChangeAccess = 'dl_IT'
    FullAccess = 'Administrators'
    Description = 'IT file share'
}
New-SmbShare @Fileshare

$Fileshare = @{
    Name = 'Nettverk'
    Path = 'C:\fileshares\Nettverk'
    ChangeAccess = 'dl_Nettverk'
    FullAccess = 'Administrators'
    Description = 'Nettverk file share'
}
New-SmbShare @Fileshare

$Fileshare = @{
    Name = 'Applikasjon'
    Path = 'C:\fileshares\Applikasjon'
    ChangeAccess = 'dl_Applikasjon'
    FullAccess = 'Administrators'
    Description = 'Applikasjon file share'
}
New-SmbShare @Fileshare

$Fileshare = @{
    Name = 'Brukerstøtte'
    Path = 'C:\fileshares\Brukerstøtte'
    ChangeAccess = 'dl_Brukerstøtte'
    FullAccess = 'Administrators'
    Description = 'Brukerstøtte file share'
}
New-SmbShare @Fileshare