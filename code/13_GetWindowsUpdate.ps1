# This script gets informasjon about
# Every computers current windows installation
# The script assumes that Powershell-Remoting is enabled
# Run the script as administrator
# Some help from chatgpt with the
#       'Get-CimInstance -Class "win32_quickfixengineering"' cmdlet

$session = Get-ADComputer -Filter * | Foreach-Object{
    New-PSSession -ComputerName $_.Name
}

$result = Invoke-Command -Session $session { Get-CimInstance -ClassName "win32_quickfixengineering" }

Remove-PSSession $session

$result | Select-Object PSComputerName, InstalledOn, HotFixID | Sort-Object InstalledOn
