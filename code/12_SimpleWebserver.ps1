# This script starts a simple http webserver
# This script is made with the help of ChatGPT

# Creates website folder and changes current working directory
New-Item -Path "C:\" -Name "webside" -ItemType Directory
Set-Location -Path C:\webside

# Gets the html file from our repository
curl https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/index.html

# Set the domain name to listen on
$domain = "onoff.ddns.net"

# Set the path to the HTML file to serve
$htmlFilePath = "C:\webside\index.html"

# Read the contents of the HTML file
$html = Get-Content $htmlFilePath -Raw

# Create a HttpListener object to handle incoming requests
$listener = New-Object System.Net.HttpListener
$listener.Prefixes.Add("http://$domain/")

# Start listening for incoming requests
$listener.Start()

Write-Output "Web server running on $domain. Close powershell to close server"

# Handle incoming requests
while ($listener.IsListening) {
    $context = $listener.GetContext()
    $response = $context.Response

    # Set the response content type and body
    $response.ContentType = "text/html"
    $buffer = [System.Text.Encoding]::UTF8.GetBytes($html)
    $response.ContentLength64 = $buffer.Length
    $response.OutputStream.Write($buffer, 0, $buffer.Length)
    $response.Close()
}

# Stop listening for incoming requests
$listener.Stop()