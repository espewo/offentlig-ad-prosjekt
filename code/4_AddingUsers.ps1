# This script adds the users from the file created by X_CreateUsersCSV.ps1
# Which is onoffusers.csv
# The script assumes that the script 'X_CreatingUsersCSV.ps1' is downloaded before running
# Thanks to Erik Hjelmset for providing this code, erikje@ansatt.ntnu.no

# Creating Users
.\2_CreateUsersCSV.ps1

$ADUsers = Import-Csv onoffusers.csv -Delimiter ';'
foreach ($User in $ADUsers) {
  if (!(Get-ADUser -LDAPFilter `
      "(sAMAccountName=$($User.Username))")) {
    New-ADUser `
    -SamAccountName        $User.Username `
    -UserPrincipalName     $User.UserPrincipalName `
    -Name                  $User.DisplayName `
    -GivenName             $User.GivenName `
    -Surname               $User.SurName `
    -Enabled               $True `
    -ChangePasswordAtLogon $False `
    -DisplayName           $user.Displayname `
    -Department            $user.Department `
    -Path                  $user.path `
    -AccountPassword `
    (ConvertTo-SecureString $user.Password -AsPlainText -Force)
  }
}

