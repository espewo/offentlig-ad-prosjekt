# This script maps GUIDs in a backup to GPO Display names

# Based on Microsoft security baseline script
# Credit to the developer who wrote the script
# We only used it to import our own GPOs

# rootdir, is the path to the directory containing one or more GPOs
# formatOutput will output the text formatted in a autosized table
#   or outputs it in a sorted list that can be further manipulated

param(
    [parameter(Mandatory=$true)]
    [String]
    $rootdir,

    [switch]
    $formatOutput
    )

$results = New-Object System.Collections.SortedList
Get-ChildItem -Recurse -Include backup.xml $rootdir | ForEach-Object {
    $guid = $_.Directory.Name
    $displayName = ([xml](Get-Content $_)).GroupPolicyBackupScheme.GroupPolicyObject.GroupPolicyCoreSettings.DisplayName.InnerText
    $results.Add($displayName, $guid)
}

if ($formatOutput)
{
    $results | Format-Table -AutoSize
}
else
{
    $results
}