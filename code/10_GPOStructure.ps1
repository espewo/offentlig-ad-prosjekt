#   This script Sets up the GPO structure and links the GPOs to the correct OU

Copy-GPO -SourceName "MyMSFT Windows 10 22h2 - Computer" -TargetName "MSFT Windows 10 22h2 - Computer"
Copy-GPO -SourceName "MyMSFT Windows 10 22h2 - User" -TargetName "MSFT Windows 10 22h2 - User"
Copy-GPO -SourceName "MyMSFT Windows 10 22h2 - Defender AntiVirus" -TargetName "MSFT Windows 10 22h2 - Defender AntiVirus"
Copy-GPO -SourceName "MyBackground" -TargetName "Background"
Copy-GPO -SourceName "MyPassword settings" -TargetName "Password settings"
Copy-GPO -SourceName "MyAdmin settings" -TargetName "Admin settings"
Copy-GPO -SourceName "MySoftware" -TargetName "Software"

Get-GPO -Name "MSFT Windows 10 22h2 - Computer" | New-GPLink -Target "OU=Klienter,DC=onoff,DC=co"
Get-GPO -Name "MSFT Windows 10 22h2 - User" | New-GPLink -Target "OU=Klienter,DC=onoff,DC=co"
Get-GPO -Name "MSFT Windows 10 22h2 - Defender AntiVirus" | New-GPLink -Target "OU=Klienter,DC=onoff,DC=co"
Get-Gpo -Name "Background" | New-GPLink -Target "DC=onoff,DC=co"
Get-GPO -Name "Password settings" | New-GPLink -Target "OU=Klienter,DC=onoff,DC=co"
Get-GPO -Name "Admin settings" | New-GPLink -Target "OU=Adm,OU=Klienter,DC=onoff,DC=co"
Get-GPO -Name "Software" | New-GPLink -Target "DC=onoff,DC=co"

