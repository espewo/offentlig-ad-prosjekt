# This script creates the global and domain local groups
# The AD infrastructure needs to be up for this to work
# On DC1 logged in as domain administrator

$GROUP = @{
  Name          = "g_Markedsføring"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Marketing team"
  Path          = "OU=Markedsføring,OU=Brukere,DC=onoff,DC=co"
  Description   = "Marketing administration users"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_AdmØko"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Administration"
  Path          = "OU=AdmØko,OU=Brukere,DC=onoff,DC=co"
  Description   = "Users within administration and finance"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_IT"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "IT Administrators"
  Path          = "OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Users that is responsible for a team"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Nettverk"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Networking team"
  Path          = "OU=Nettverk,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Users that work in the Networking team"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Applikasjon"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Application team"
  Path          = "OU=Applikasjon,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Users working in the application team"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Brukerstøtte"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "User support team"
  Path          = "OU=Brukerstøtte,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Users/Groups that will be allowed to RDP to CL1"
}
New-ADGroup @GROUP

#Moving users to groups
.\3_MovingUsersToGroups.ps1

# Making Domain Local groups and adds Global groups as a member of Domain Local Groups
$DL_GROUP = @{
  Name          = "dl_Markedsføring"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local Marketing Group"
  Path          = "OU=Markedsføring,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local Marketing Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

# Adds Global groups as a member of Domain Local Groups
Get-ADGroup -Identity g_Markedsføring | Add-ADPrincipalGroupMembership -MemberOf dl_Markedsføring

$DL_GROUP = @{
  Name          = "dl_AdmØko"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local Administration Group"
  Path          = "OU=AdmØko,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local Administration Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

Get-ADGroup -Identity g_AdmØko | Add-ADPrincipalGroupMembership -MemberOf dl_AdmØko

$DL_GROUP = @{
  Name          = "dl_IT"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local IT Administrators Group"
  Path          = "OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local IT Administrations Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

Get-ADGroup -Identity g_IT | Add-ADPrincipalGroupMembership -MemberOf dl_IT

$DL_GROUP = @{
  Name          = "dl_Nettverk"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local Nettverk Group"
  Path          = "OU=Nettverk,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local Nettverk Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

Get-ADGroup -Identity g_Nettverk | Add-ADPrincipalGroupMembership -MemberOf dl_Nettverk

$DL_GROUP = @{
  Name          = "dl_Applikasjon"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local Application Group"
  Path          = "OU=Applikasjon,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local Application Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

Get-ADGroup -Identity g_Applikasjon | Add-ADPrincipalGroupMembership -MemberOf dl_Applikasjon

$DL_GROUP = @{
  Name          = "dl_Brukerstøtte"
  GroupCategory = "Security"
  GroupScope    = "DomainLocal"
  DisplayName   = "Domain Local Support Group"
  Path          = "OU=Brukerstøtte,OU=IT,OU=Brukere,DC=onoff,DC=co"
  Description   = "Domain Local Brukerstøtte Group for accecessing fileshare"
}

New-ADGroup @DL_GROUP

Get-ADGroup -Identity g_Brukerstøtte | Add-ADPrincipalGroupMembership -MemberOf dl_Brukerstøtte