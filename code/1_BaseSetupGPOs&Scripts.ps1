#Downloads gpos and all the scrips

#Downloads the GPOs from git
curl -O https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/blob/main/GPOs.zip

#Downlaods msi installers for Notepad++ and Teams
curl -O https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/blob/main/Notepad%2B%2B7_9_1.msi
curl -O https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/blob/main/Teams_windows_x64.msi

#Renames \Notepad%2B%2B7_9_1.msi to Notepad++7_9_1.msi(url uses %2B instead of +)
Rename-Item -Path .\Notepad%2B%2B7_9_1.msi -NewName .\Notepad++7_9_1.msi

#Downloads images used for Homescreen and Loginscreen
curl -O https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/blob/main/img/HomeScreen.jpg
curl -O https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/blob/main/img/LoginScreen.jpg

#Creates folders to hold these files
New-Item -Name "Software" -ItemType Directory -Path "\\onoff.co\SYSVOL\onoff.co\"
New-Item -Name "Img" -ItemType Directory -Path "\\onoff.co\SYSVOL\onoff.co\"

#Moves newly downloaded files to assigned folders
Move-Item .\Notepad++7_9_1.msi \\onoff.co\SYSVOL\onoff.co\Software\
Move-Item .\Teams_windows_x64.msi \\onoff.co\SYSVOL\onoff.co\Software\
Move-Item .\HomeScreen.jpg \\onoff.co\SYSVOL\onoff.co\Img
Move-Item .\LoginScreen.jpg \\onoff.co\SYSVOL\onoff.co\Img


#Downloading the rest of the scripts

#All script URLs
$urler = @('https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/0_BaseSetup.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/dcsg-1005-project/-/raw/main/code/1_BaseSetupGPOs&Scripts.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/13_GetWindowsUpdate.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/2_CreatingOUs.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/3_MovingToOUs.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/4_AddingUsers.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/5_CreateUsersCSV.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/6_MakingGroups.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/7_MovingUsersToGroups.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/8_GPOImport.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/10_GPOStructure.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/9_MapGuidToGpoName.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/11_CreateSmbShares.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/12_SimpleWebserver.ps1',
           'https://gitlab.stud.idi.ntnu.no/espewo/offentlig-ad-prosjekt/-/raw/main/code/index.html'
           )

#Installing all the scripts
foreach($url in $urler){
    curl -O $url
}

Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass

exit
